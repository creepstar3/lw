#include <boost/filesystem.hpp>
#include <iostream>
#include <string>
#include <vector>

class BillInfo {

public:

    boost::filesystem::path Path;
    int capacity;
    void capacityAdd();
    BillInfo(const boost::filesystem::path path);
};

BillInfo::BillInfo(const boost::filesystem::path path) {
    this->Path = path;
    capacity = 0;
}

void BillInfo::capacityAdd() {
    ++capacity;
}


class boostsearch {

private:
    static std::string fileNameTemplate;

    boost::filesystem::path Path;
    std::vector<BillInfo> lastBrokerFiles;

    void search_directory(boost::filesystem::path Path);
    bool overholdelse(std::string currentFile);

public:
    boostsearch() = delete;
    boostsearch(boost::filesystem::path Path);
    void console();
};

std::string boostsearch::fileNameTemplate = "balance_XXXXXXXX_XXXXXXXX.txt";

boostsearch::boostsearch(boost::filesystem::path Path) {
this->Path = Path;
search_directory(Path);
}

void boostsearch::console(){
    for(const auto& [Path, capacity]: lastBrokerFiles) {
        std::cout
        << "broker: "
        << Path.parent_path().filename()
        << " account: "
        << Path.filename().string().substr(8,8)
        << " files:"
        << capacity
        << " lastdate: "
        << Path.filename().string().substr(17,8)
        << '\n';
    }
}

bool boostsearch::overholdelse(std::string currentFile) {
    for (int i = 0; i < fileNameTemplate.size(); ++i) {
        //std::cout << i << ':' << currentFile[i] << ' ' << fileNameTemplate[i] << '\n';
        if(fileNameTemplate[i] != 'X') {
            if(currentFile[i] != fileNameTemplate[i]) {
                return false;
            }
        }
        else {
            continue;
        }
    }
    return true;
}

void boostsearch::search_directory(boost::filesystem::path Path) {
    if (exists(Path)) {
        std::string currentDirectory = Path.filename().string();
        for (const boost::filesystem::directory_entry &directoryEntry : boost::filesystem::directory_iterator{Path}) {
            if (boost::filesystem::is_directory(directoryEntry.path())) {
                search_directory(directoryEntry.path());
            }
            if (boost::filesystem::is_regular_file(directoryEntry.path())) {
                std::string currentFile = directoryEntry.path().filename().string();
                if (currentFile.size() == fileNameTemplate.size()) {
                    if (boost::filesystem::extension(directoryEntry.path()) == ".txt") {
                        if (overholdelse(currentFile)) {

                            std::cout << currentDirectory << ' ';
                            std::string currentFile = directoryEntry.path().filename().string();
                            std::cout << boost::filesystem::extension(directoryEntry.path()) << ' ';
                            std::cout << directoryEntry.path().filename().string() << '\n';

                            BillInfo newBill(directoryEntry.path());
                            newBill.capacityAdd();
                            lastBrokerFiles.push_back(newBill);

                            bool rewrite = false;
                            for (auto iterator: lastBrokerFiles) {
                                if (directoryEntry.path().parent_path() == iterator.Path.parent_path()) {

                                    //std::cout << directoryEntry.path().filename().string()<< '\n';
                                    //std::cout << iterator.Path.filename().string() << "\n\n";

                                    if (directoryEntry.path().filename().string().substr(8, 8) == iterator.Path.filename().string().substr(8, 8)) {
                                        if (std::stoi(directoryEntry.path().filename().string().substr(17, 8)) > std::stoi(iterator.Path.filename().string().substr(17, 8))) {

                                            iterator.Path = newBill.Path;
                                            newBill.capacity = iterator.capacity;
                                            newBill.capacityAdd();

                                            lastBrokerFiles.pop_back();
                                            lastBrokerFiles.push_back(newBill);

                                            for (auto jterator = lastBrokerFiles.begin(); jterator!=lastBrokerFiles.end(); ++jterator) {
                                                if (directoryEntry.path().filename().string().substr(8, 8) == jterator->Path.filename().string().substr(8, 8)) {
                                                    lastBrokerFiles.erase(jterator);
                                                    break;
                                                }
                                            }
                                            break;
                                        }
                                        else {
                                            iterator.capacityAdd();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

int main(int argc, char *argv[]) {
    boost::filesystem::path Path{"C:/Users/creepstar3/Desktop/LW AL/Sem3/filesystem/ftp"};
    boostsearch directory(Path);
    std::cout << '\n' << '\n';
    directory.console();
    return 0;
}