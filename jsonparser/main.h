#include <iostream>
#include <any>
#include <vector>
#include <string>
#include <map>
#include <fstream>
#include <exception>

class JSON
{
private:
    std::map<std::string, std::any> object;
    std::vector<std::any> array;
public:
    JSON(); // Default constructor
    JSON(const std::string &s, int &position, bool isFile); // Constructor with parameters
    bool is_array(); // Data type checking function
    bool is_object(); // Data type checking function
    std::any &operator[](const std::string &key); // Getting value by key in object function
    std::any &operator[](int index); // Getting alue by index in array function
    static std::string parseString(const std::string &s, int &position); // String value processing function
    static bool parseBool(const std::string &s, int &position); // Bool value processing function
    static double parseNumber(const std::string &s, int &position); // Number value processing function
    static std::map<std::string, std::any> parseObject(const std::string &s, int &position); // Object processing function
    static std::vector<std::any> parseArray(const std::string &s, int &position); // Array processing function
    void parse(const std::string &s, int &position); // Input line processing function
    void parseFile(const std::string &path);  // Input file processing function
};