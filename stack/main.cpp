#pragma once
#include <iostream>

struct Object {
    double data;
    Object* next;
    Object() {
        data = 0;
        next = nullptr;
    }
};

template <typename T>
class Stack {
private:
    Object* hd;
public:
    Stack() {
        hd = nullptr;
    }

    void push(T&& value) {

        Object* A = new Object();
        A->data = std::move(value);
        if (hd == nullptr) {

            hd = A;
        }
        else {
            A->next = hd;
            hd = A;
        }

    }

    void push(const T& value) {
        Object* A = new Object();
        A->data = value;
        if (hd == nullptr) {
            hd = A;
        }
        else {
            A->next = hd;
            hd = A;
        }
    }

    void pop() {
        Object* buf = new Object();
        hd = std::move(hd->next);
    }
    const T& head() const {
        return hd->data;
    }

    template <typename ... Args>
    void push_emplace(Args&&... value) {
        Object* A = new Object();
        A->data = T(std::forward<Args>(value)...);
        if (hd == nullptr) {
            hd = A;
        }
        else {
            A->next = hd;
            hd = A;
        }
    }

    void print() {
        std::cout << hd->data << std::endl;
        Object* buffer = hd;
        while (buffer->next != nullptr) {
            std::cout << buffer->next->data << std::endl;
            buffer = buffer->next;
        }
        std::cout << std::endl;
    }
};