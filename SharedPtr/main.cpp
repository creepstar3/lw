#include <atomic>
#include <iostream>
#include <memory>

class Control {
    std::atomic_uint counter;
public:
    Control() {
        counter = 0;
    }

    void increase() {
        counter++;
    }

    void decrease() {
        counter--;
    }

    size_t size() {
        return static_cast<size_t>(counter);
    }
    ~Control() = default;
};

template<typename T>
class SharedPtr {
    T *data;
    Control *cont;
public:
    SharedPtr() {
        data = nullptr;
        cont = nullptr;
    }

    SharedPtr(T *r) {
        data = r;
        cont = new Control();
        cont->increase();
    }

    SharedPtr(const SharedPtr &r) {
        cont = r.cont;
        data = r.data;
        if (cont != nullptr) {
            cont->increase();
        }
    }

    SharedPtr(SharedPtr &&r) {
        cont = r.cont;
        data = r.data;
        cont->increase();
        r.cont = nullptr;
        r.data = nullptr;
    }

    auto operator=(const SharedPtr &r) -> SharedPtr & {
        if (data != nullptr) {
            cont->decrease();
            if (cont->size() == 0) {
                delete cont;
                delete data;
            }
        }

        cont = r.cont;
        if (cont != nullptr) cont->increase();
        data = r.data;
        return (*this);
    }

    auto operator=(SharedPtr &&r) -> SharedPtr & {
        if (data != nullptr) {
            cont->decrease();
            if (cont->size() == 0) {
                delete cont;
                delete data;
            }
        }
        cont = r.cont;
        data = r.data;
        if (r.data != nullptr) {
            delete r.cont;
            delete r.data;
        }
        return (*this);
    }

    operator bool() const {
        if (data == nullptr) return false;
        return cont->size() != 0;
    }

    auto operator*() const -> T & {
        return *data;
    }
    auto operator->() const -> T * {
        return data;
    }

    auto get() -> T * {
        return data;
    }

    void reset() {
        if (cont != nullptr) {
            cont->decrease();
            if (cont->size() == 0) {
                delete data;
                delete cont;
            }
        }
        data = nullptr;
        cont = nullptr;
    }

    void reset(T* ptr) {
        reset();
        data = ptr;
        cont = new Control();
        cont->increase();
    }

    void swap(SharedPtr &r) {
        auto buf = r.data;
        r.data = data;
        data = buf;
        auto buf2 = r.cont;
        r.cont = cont;
        cont = buf2;
    }

    auto use_count() const -> size_t {
        if (!*this) return 0;
        return cont->size();
    }

    ~SharedPtr() {
        if (data == nullptr || cont == nullptr) return;
        if (*this) {
            cont->decrease();
        }
        if (cont->size() == 0){
            delete cont;
            delete data;
        }
    }
};
int main(){

    printf("ORIGINAL\r\n");
    int* po = new int(5);
    std::shared_ptr<int> ptro1(po);
    std::shared_ptr<int> ptro2(ptro1);
    std::shared_ptr<int> ptro3(ptro2);
    std::shared_ptr<int> ptro4(ptro3);
    std::cout << ptro1.use_count() << std::endl;
    ptro3.reset();
    ptro4.reset();
    {
        std::shared_ptr<int> ptro5(ptro4);
        std::cout << ptro1.use_count() << std::endl;
        std::cout << ptro5.use_count() << std::endl;
    }
    std::cout << ptro2.use_count() << std::endl;

    printf("MY SHARED\r\n");
    int* p = new int(5);
    SharedPtr<int> ptr1(p);
    SharedPtr<int> ptr2(ptr1);
    SharedPtr<int> ptr3(ptr2);
    SharedPtr<int> ptr4(ptr3);
    std::cout << ptr1.use_count() << std::endl;
    ptr3.reset();
    ptr4.reset();
    {
        SharedPtr<int> ptr5(ptr4);
        std::cout << ptr1.use_count() << std::endl;
        std::cout << ptr5.use_count() << std::endl;
    }
    std::cout << ptr2.use_count() << std::endl;
    return 0;
}
