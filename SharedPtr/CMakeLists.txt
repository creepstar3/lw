cmake_minimum_required(VERSION 3.15)
project(SharedPtr)

set(CMAKE_CXX_STANDARD 20)

add_executable(SharedPtr main.cpp)